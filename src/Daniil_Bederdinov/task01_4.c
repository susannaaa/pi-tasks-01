#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
int main()
{
    char str[256];
    int len = 0;
    printf("Enter a number: \n");
    fgets(str, 256, stdin);
    len = (strlen(str)-1)%3;
    for (int i = 0; i < strlen(str); i++)
    {
        printf("%c", str[i]);
        if ((i + 1) % 3 == len)
            printf(" ");
    }
    
    return 0;
}